`ifndef MERGER_RR_ARBITRER
 `define MERGER_RR_ARBITRER


interface AuroraDataInterface;
   wire [7:0][31:0] DataEOC;
   //pragma coverage toggle = off
   wire [7:0] 	    DataMask;
   wire 	    DataEOC_EOF;
   //pragma coverage toggle = on
   wire 	    DataEOC_empty;
   logic 	    DataEOC_read;

   modport in (
	       input DataEOC, DataMask, DataEOC_empty, DataEOC_EOF,
	       output DataEOC_read
	       );

   modport out (
		output DataEOC, DataMask, DataEOC_empty, DataEOC_EOF,
		input DataEOC_read
	       );
   
endinterface // AuroraDataInterface


module MergerRRArbitrer
  #(
    INPUTS = 4
    )
  (
   input wire Reset_b,
   input wire Clock,
	      AuroraDataInterface.in Input [INPUTS],
   AuroraDataInterface.out Output
			);

   typedef struct {
      logic [7:0][31:0] DataEOC;
      //pragma coverage toggle = off
      logic [7:0] 	DataMask;
      //pragma coverage toggle = on
      logic 		DataEOC_empty;
      logic 		DataEOC_read;
   } fifoInterface;

   fifoInterface intFIFOSignals [INPUTS];
   fifoInterface intOutput;
   
   
   logic [INPUTS-1:0] currentSel;
   logic [INPUTS-1:0] currentSelDelay;
   logic [INPUTS-1:0] nextSel;
   logic [INPUTS-1:0] currentAvailable;
   logic [INPUTS-1:0] selPriority [INPUTS];
   logic 	      intRead [INPUTS];
   
   
   genvar 	      genChan;

   for (genChan = 0; genChan < INPUTS; genChan++) begin
      assign Input[genChan].DataEOC_read = intFIFOSignals[genChan].DataEOC_read;
      assign intFIFOSignals[genChan].DataEOC = Input[genChan].DataEOC;
      assign intFIFOSignals[genChan].DataEOC_empty = Input[genChan].DataEOC_empty;
      //pragma coverage toggle = off
      assign intFIFOSignals[genChan].DataMask = Input[genChan].DataMask;
      //pragma coverage toggle = on
      assign intFIFOSignals[genChan].DataEOC_read = intRead[genChan];
   end

   assign Output.DataEOC = intOutput.DataEOC;
   //pragma coverage toggle = off
   assign Output.DataMask = intOutput.DataMask;
   //pragma coverage toggle = on
   assign intOutput.DataEOC_read = Output.DataEOC_read;
   assign Output.DataEOC_empty = intOutput.DataEOC_empty;
   
   always_comb
     begin
	intOutput.DataEOC = intFIFOSignals[0].DataEOC;
   //pragma coverage toggle = off
	intOutput.DataMask = intFIFOSignals[0].DataMask;
   //pragma coverage toggle = on
	intOutput.DataEOC_empty = intFIFOSignals[0].DataEOC_empty;
	for (int unsigned inChan = 0; inChan < INPUTS; inChan++)
	  intRead[inChan] = 1'b0;
	
	for (int unsigned inChan = 0; inChan < INPUTS; inChan++) begin
	end
	
	for (int unsigned inChan = 0; inChan < INPUTS; inChan++) begin
	   if (currentSelDelay == (1 << inChan)) begin
	      intOutput.DataEOC = intFIFOSignals[inChan].DataEOC;
         //pragma coverage toggle = off
	      intOutput.DataMask = intFIFOSignals[inChan].DataMask;
         //pragma coverage toggle = on
	   end
	end
	
	for (int unsigned inChan = 0; inChan < INPUTS; inChan++) begin
	   if (currentSel == (1 << inChan)) begin
	      intOutput.DataEOC_empty = intFIFOSignals[inChan].DataEOC_empty;
	      intRead[inChan] = intOutput.DataEOC_read;
	   end
	end
     end // always_comb

   always_comb
     for (int unsigned inChan = 0; inChan < INPUTS; inChan++)
       currentAvailable[inChan] = ~intFIFOSignals[inChan].DataEOC_empty;

   logic [INPUTS-1:0] currentAvailableRot [INPUTS];
   logic [INPUTS-1:0] higherPriorityRot [INPUTS];
   logic [INPUTS-1:0] selPriorityRot [INPUTS];

   genvar 	      innerBits;
   
   for (genChan = 0; genChan < INPUTS; genChan++)
     begin
	if (genChan == INPUTS-1) begin
	   assign currentAvailableRot[genChan] = currentAvailable;
        end else begin
	   assign currentAvailableRot[genChan] = (currentAvailable >> (genChan+1)) | (currentAvailable << (INPUTS-(genChan+1)));
	end
	assign higherPriorityRot[genChan][0] = 1'b0;
	for (innerBits = 1; innerBits < INPUTS; innerBits++)
	  begin
	     assign higherPriorityRot[genChan][innerBits] = (higherPriorityRot[genChan][innerBits-1] | currentAvailableRot[genChan][innerBits-1]);
	  end
	assign selPriorityRot[genChan] = currentAvailableRot[genChan] & ~higherPriorityRot[genChan];
	if (genChan == 7) begin
	   assign selPriority[genChan] = selPriorityRot[genChan];
	end else begin
	   assign selPriority[genChan] = (selPriorityRot[genChan] << (genChan+1)) | (selPriorityRot[genChan] >> (INPUTS-(genChan+1)));
	end
     end
   
   always_comb
     begin
	nextSel = 'b1;
	for (int inChan = 0; inChan < INPUTS; inChan++)
	  if (currentSel == (1 << inChan))
	    if (selPriority[inChan] != '0)
	      nextSel = selPriority[inChan];
     end
   

   logic firstNotEmpty;
   logic currentNotEmpty;
   assign currentNotEmpty = | ( currentSel & currentAvailable );
   
   always_ff @(posedge Clock) 
     begin
	if (Reset_b == 1'b0) begin
	   currentSel <= 'b1;
	   currentSelDelay <= 'b1;
	   firstNotEmpty <= 'b0;
	end else begin
	   firstNotEmpty <= (currentNotEmpty ^ firstNotEmpty) & currentNotEmpty;
	   if (intOutput.DataEOC_read | ~currentNotEmpty) begin
	      currentSel <= nextSel;
	   end	   
	   currentSelDelay <= currentSel;
	end
     end
   
endmodule // MergerRRArbitrer


`endif
