
`ifndef AURORA_PERIODIC_FSM__SV   // include guard
`define AURORA_PERIODIC_FSM__SV

`timescale 1ns / 1ps

`include "rtl/eoc/aurora/AuroraDefines.sv"

module AuroraPeriodicFSM #( parameter WTS_WIDTH = 4,
   parameter BTS_WIDTH = 4

   ) ( input wire LaneReady,
    
    output logic SendBlock,
    input wire BlockSent,

    input wire [WTS_WIDTH-1:0] WaitToSend,
    input wire [BTS_WIDTH-1:0] BlocksToSend,

    input wire Clk,
    input wire Rst_b
    ) ;
      // Defining outsdie process to make these signals visible to SVAs
      logic [WTS_WIDTH-1:0] wait_counter;
      logic [BTS_WIDTH-1:0] send_counter;

   enum        {NOT_READY, WAIT, SEND} fsm_state;//synopsys keep_signal_name "fsm_state"

   always_ff @(posedge Clk) begin
      if (Rst_b == 1'b0) begin
	 wait_counter <= WTS_WIDTH'(0);
	 send_counter <= BTS_WIDTH'(0);
	 fsm_state <= NOT_READY;
	 SendBlock <= 1'b0;
      end else begin
	 case(fsm_state)
	   NOT_READY : begin
	      SendBlock <= 1'b0;
	      wait_counter <= WTS_WIDTH'(0);
	      send_counter <= BTS_WIDTH'(0);
	      if (LaneReady)
		fsm_state <= WAIT;
	   end
	   WAIT : begin
	      SendBlock <= 1'b0;
	      send_counter <= BTS_WIDTH'(0);
	      if (~LaneReady)
		fsm_state <= NOT_READY;
	      else if (wait_counter < WaitToSend | WaitToSend == {WTS_WIDTH{1'b1}}) begin
		 wait_counter <= WTS_WIDTH'(wait_counter + 1);
	      end else begin
		fsm_state <= SEND;
	      end
	   end
	   SEND : begin
	      wait_counter <= WTS_WIDTH'(0);
	      SendBlock <= 1'b1;
	      if (~LaneReady)
		fsm_state <= NOT_READY;
	      else if (send_counter < BlocksToSend) begin
		 if (BlockSent)
		   send_counter <= BTS_WIDTH'(send_counter + 1);
	      end else begin
		fsm_state <= WAIT;
	      end	      
	   end
	 endcase
      end // else: !if(Rst)      
   end // always_ff @ (posedge Clk or posedge Rst)

//---------------------------------//Assertions and properties to test//---------------------------------//---------------------------------
  //synopsys translate_off
  
default clocking @(posedge Clk); endclocking

    property S_IDLE  ;
    disable iff (Rst_b)       (Rst_b == 1'b0)                                    |-> ##1 (fsm_state == NOT_READY) ;  
    endproperty 
     
    property S_NOTREADY ;
    disable iff (Rst_b == 1'b0)      (fsm_state == NOT_READY) ##0 (LaneReady == 1'b0)  |-> (fsm_state == NOT_READY) ;  
    endproperty 
     
    property S_NOTREADY_else  ;
    disable iff (Rst_b == 1'b0)      (fsm_state == NOT_READY) ##0 (LaneReady == 1'b1)  |-> (fsm_state == WAIT) ;  
    endproperty                       
      
    property S_WAIT  ;
    disable iff (Rst_b == 1'b0)      (fsm_state == WAIT)      ##0 (LaneReady == 1'b0)  |-> ##1 (fsm_state == NOT_READY) ;  
    endproperty       
       
    property S_WAIT_else  ;
    disable iff (Rst_b == 1'b0) (fsm_state == WAIT) ##0 ((wait_counter < WaitToSend | WaitToSend == {WTS_WIDTH{1'b1}})) |-> (fsm_state == WAIT);  
    endproperty          

     
    property INC1_WAIT_CNT  ;   
    disable iff (Rst_b == 1'b0)      ((fsm_state == WAIT) ##0 ((wait_counter < WaitToSend )|(WaitToSend == {WTS_WIDTH{1'b1}}))) |-> ##1 wait_counter == $past(wait_counter)+1'b1;
    endproperty   
     
    property S_SEND;   
    disable iff (Rst_b == 1'b0)     (fsm_state == SEND) ##0 (LaneReady == 1'b0) |-> ##1 (fsm_state == NOT_READY) ; 
    endproperty 

    property S_SEND_else;   
    disable iff (Rst_b == 1'b0)     (fsm_state == SEND) ##0 (BlocksToSend < send_counter)  ##0 (BlockSent == 1'b1) |-> ##1 (fsm_state == WAIT) ; 
    endproperty      

    property S_SEND_COUNT;   
    disable iff (Rst_b == 1'b0)     ((fsm_state == SEND) ##0 (send_counter < BlocksToSend) ##0 (BlockSent == 1'b1)) |-> ##1 send_counter == $past(send_counter)+1'b1;
    endproperty  
//---------------------------------//---------------------------------//---------------------------------//---------------------------------
 `ifdef SVA_EN
    generate 
    begin : SVA_APeriodc_FSM     
     ERR_S_DEFAULT_STATE             : assert property  (S_IDLE);          // Reset msut get the state machine back to default "NOT_READY" in here      
     ERR_S_LANE_READY                : assert property  (S_NOTREADY);      // Stay in NOT_READY if lane is not ready
     ERR_S_WAIT_READY                : assert property  (S_WAIT);          // Transition from WAIT to NOT_READY if lane is no more ready.
     ERR_S_WAIT_ELSE                 : assert property  (S_WAIT_else);     // From wait state to SEND if lane stayed ready
     ERR_S_WAIT_COUNT                : assert property  (INC1_WAIT_CNT);   // incrementing wait counter
     ERR_S_SEND_READY                : assert property  (S_SEND);          // Transition from SEND to NOT_READY if lane is no more ready. 
     ERR_S_SEND_ELSE                 : assert property  (S_SEND_else);     // IF Lane is ready then move to wait    
     ERR_S_SEND_COUNT                : assert property  (S_SEND_COUNT);    // Send counter increment   
//---------------------------------//---------------------------------//---------------------------------//---------------------------------
   end
   endgenerate
  `endif
  //synopsys translate_on

endmodule : AuroraPeriodicFSM

`endif   // AURORA_PERIODIC_FSM__SV

