
`ifndef AURORA_MULTILANE_NEW_DATA_FSM__SV
`define AURORA_MULTILANE_NEW_DATA_FSM__SV

`timescale 1ns/1ps

`include "rtl/eoc/aurora/AuroraDefines.sv"

module AuroraMultilaneNewDataFSM (

   input wire 		    LaneReady,
   input wire 		    FIFO_Empty,
   input wire [7:0][31:0]   FIFO_Data,
   input wire [7:0] 	    FIFO_DataMask,
   input wire 		    EndOfFrame,
   input wire 		    BlockSent,

   input wire [3:0] 	    ActiveLanes,

   input wire 		    Clk,
   input wire 		    Rst_b,

   output logic 	    Ready, 
   output logic 	    FIFO_Read,
   output logic [3:0][63:0] DataToSend,
   output logic 	    SendBlock,
   output logic [3:0][3:0]  BytesToSend);

   enum 		    {NOT_READY, NO_DATA, PREPARE_READ, SEND_DATA, STORE, WAIT_TO_SEND, WAIT_TO_SEND_EOF, SEND_REMAINDER, STORE_REMAINDER} data_fsm_state;
   
   logic [3:0][63:0] 			stored_datatosend;
   logic [7:0] 				stored_datamask;
   logic 				stored_eof;
   logic 				stored_flag_narrowpath;

   logic 				flag_eof;
   logic 				flag_stored;

   logic 				flag_narrowpath;
   logic [3:0][63:0] 			remainderData;
   logic [7:0] 				remainderDataMask;
   

   function automatic logic [3:0][3:0] calcBytesToSend 
     (input logic [3:0] datamask, 
      input logic [3:0] activedatamask);

      calcBytesToSend[0] = activedatamask[0] ?
			   (datamask[0] ? `FULL_DATA : `ZERO_DATA)
			   : `DISABLE_DATA;
      calcBytesToSend[1] = activedatamask[1] ?
			   (datamask[1] ? `FULL_DATA 
			    : (datamask[0] ? `ZERO_DATA : `DISABLE_DATA))
			   : `DISABLE_DATA;
      calcBytesToSend[2] = activedatamask[2] ?
			   (datamask[2] ? `FULL_DATA 
			    : (datamask[1] ? `ZERO_DATA : `DISABLE_DATA))
			   : `DISABLE_DATA;
      calcBytesToSend[3] = activedatamask[3] ?
			   (datamask[3] ? `FULL_DATA 
			    : (datamask[2] ? `ZERO_DATA : `DISABLE_DATA))
			   : `DISABLE_DATA;
   endfunction // calcBytesToSend

   function automatic logic [3:0] lane_data_mask(input logic [7:0] datamask);
      // It assumes that data is *always* aligned to 64 bits
      lane_data_mask[0] = datamask[0];
      lane_data_mask[1] = datamask[2];
      lane_data_mask[2] = datamask[4];
      lane_data_mask[3] = datamask[6];      
   endfunction // lane_data_mask

   assign flag_narrowpath = | (lane_data_mask(FIFO_DataMask) & ~ActiveLanes);
   
   always_ff @(posedge Clk) begin
      if (Rst_b == 1'b0) begin
	 data_fsm_state <= NOT_READY;
	 SendBlock <= 1'b0;
	 flag_eof <= 1'b0;
	 flag_stored <= 1'b0;
	 
      end else begin
	 // Defaults

	 if (BlockSent) begin
	    SendBlock <= 1'b0;
	 end
	 FIFO_Read <= 1'b0;

	 // State Machine
	 case(data_fsm_state)
	   NOT_READY : begin
	      if (FIFO_Empty) begin
		 data_fsm_state <= NO_DATA;
	      end else begin
		 data_fsm_state <= PREPARE_READ;
		 FIFO_Read <= 1'b1;
	      end
	   end

	   NO_DATA : begin
	      if (~FIFO_Empty) begin // if there is data prepare the read
		 data_fsm_state <= PREPARE_READ;
		 FIFO_Read <= 1'b1;
	      end
	   end
	   
	   PREPARE_READ : begin
	      // data will be valid at the next cycle so
	      if (~SendBlock | BlockSent) begin // if we are not sending or just finished go to send the new data ...
		 data_fsm_state <= SEND_DATA;
		 FIFO_Read <= 1'b1; // we might read an empty FIFO, but it is to prefetch data
	      end else begin // ... otherwise go to store it
		 data_fsm_state <= STORE;
	      end
	   end

	   SEND_DATA : begin
	      if (flag_narrowpath) begin
		 // if data to send is wider than the available channels
		 // fall back to single-lane used
		 // NOT OPTIMIZED IN CASE OF >1 active lanes
		 // but it shouldn't be a realistic case

		 DataToSend <= {192'h0,FIFO_Data[0+:2]};
		 BytesToSend <= {`DISABLE_DATA,`DISABLE_DATA,`DISABLE_DATA,`FULL_DATA};
		 flag_eof <= EndOfFrame;
		 remainderData <= {64'h0,FIFO_Data[2+:6]};
		 remainderDataMask <= {2'h0,FIFO_DataMask[2+:6]};
		 if (~FIFO_Empty)
		   data_fsm_state <= STORE_REMAINDER;
		 else
		   data_fsm_state <= SEND_REMAINDER;
		 SendBlock <= 1'b1;
	      end else begin
		 // Register the data to be sent
		 DataToSend <= FIFO_Data;
		 BytesToSend <= calcBytesToSend(lane_data_mask(FIFO_DataMask), ActiveLanes);
		 flag_eof <= (EndOfFrame & (lane_data_mask(FIFO_DataMask) == ActiveLanes));
	      
		 // Give command to send data
		 SendBlock <= 1'b1;

		 // If there are more data to read prefetch it, we still have ~3 cycles to wait (66/20)
		 if (~FIFO_Empty) begin
		    data_fsm_state <= STORE;
		 end else begin // or else go back to no data or send EOF
		    if (EndOfFrame & (lane_data_mask(FIFO_DataMask) == ActiveLanes))
		      data_fsm_state <= WAIT_TO_SEND_EOF;
		    else
		      data_fsm_state <= NO_DATA;
		 end
	      end
	   end

	   STORE : begin
	      // Store data to be ready to send it when possible
	      stored_datatosend <= FIFO_Data;
	      stored_datamask <= FIFO_DataMask;
	      stored_eof <= (EndOfFrame & (lane_data_mask(FIFO_DataMask) == ActiveLanes));
	      stored_flag_narrowpath <= flag_narrowpath;
	      flag_stored <= '1;
	      if (flag_eof)
		data_fsm_state <= WAIT_TO_SEND_EOF;
	      else
		data_fsm_state <= WAIT_TO_SEND;
	   end

	   STORE_REMAINDER : begin
	      // Store data to be ready to send it when possible
	      stored_datatosend <= FIFO_Data;
	      stored_datamask <= FIFO_DataMask;
	      stored_eof <= (EndOfFrame & (lane_data_mask(FIFO_DataMask) == ActiveLanes));
	      stored_flag_narrowpath <= flag_narrowpath;
	      flag_stored <= '1;
	      data_fsm_state <= SEND_REMAINDER;
	   end

	   WAIT_TO_SEND : begin
	      if (BlockSent | ~SendBlock) begin // if the current block has been sent immediately send a new one on the next cycle
		 flag_stored <= '0;
		 if (stored_flag_narrowpath) begin
		    // if data to send is wider than the available channels
		    // fall back to single-lane used
		    // NOT OPTIMIZED IN CASE OF >1 active lanes
		    // but it shouldn't be a realistic case

		    DataToSend <= {192'h0,stored_datatosend[0+:1]};
		    BytesToSend <= {`DISABLE_DATA,`DISABLE_DATA,`DISABLE_DATA,`FULL_DATA};
		    flag_eof <= stored_eof;
		    remainderData <= {64'h0,stored_datatosend[1+:3]};
		    remainderDataMask <= {2'h0,stored_datamask[2+:6]};
		    data_fsm_state <= SEND_REMAINDER;
		    SendBlock <= 1'b1;
		 end else begin
		    DataToSend <= stored_datatosend;
		    BytesToSend <= calcBytesToSend(lane_data_mask(stored_datamask), ActiveLanes);
		    SendBlock <= 1'b1;
		    if (stored_eof)
		      data_fsm_state <= WAIT_TO_SEND_EOF;
		    else if (~FIFO_Empty) begin // if it's possible can fetch new data
		       FIFO_Read <= 1'b1;
		       data_fsm_state <= PREPARE_READ;
		    end else begin 
		       data_fsm_state <= NO_DATA;
		    end
		 end
	      end
	   end // case: WAIT_TO_SEND

	   WAIT_TO_SEND_EOF : begin
	      if (BlockSent | ~SendBlock) begin // if the current block has been sent immediately send a new one on the next cycle
		 flag_eof <= 1'b0;
		 DataToSend[0] <= 64'h000046544b203c33;
		 DataToSend[3:1] <= '0;
		 BytesToSend[0] <= 4'h0;
		 BytesToSend[1] <= `DISABLE_DATA;
		 BytesToSend[2] <= `DISABLE_DATA;
		 BytesToSend[3] <= `DISABLE_DATA;		 
		 SendBlock <= 1'b1;
		 if (flag_stored)
		   data_fsm_state <= WAIT_TO_SEND;
		 else if (~FIFO_Empty) begin // if it's possible can fetch new data
		    FIFO_Read <= 1'b1;
		    data_fsm_state <= PREPARE_READ;
		 end else begin 
		    data_fsm_state <= NO_DATA;
		 end		 
	      end
	   end

	   SEND_REMAINDER : begin 
	      if (BlockSent | ~SendBlock) begin // if the current block has been sent immediately send a new one on the next cycle
		 if ( | (lane_data_mask(remainderDataMask) & ~ActiveLanes) ) begin
		    // if there is still data that doesn't fit in the available lanes
		    DataToSend <= {192'h0,remainderData[0+:1]};
		    BytesToSend <= {`DISABLE_DATA,`DISABLE_DATA,`DISABLE_DATA,`FULL_DATA};
		    remainderData <= {64'h0,remainderData[1+:3]};
		    remainderDataMask <= {2'h0,remainderDataMask[2+:6]};
		    SendBlock <= 1'b1;
		 end else begin
		    // if it fits send the remainder
		    DataToSend <= remainderData;
		    BytesToSend <= calcBytesToSend(lane_data_mask(remainderDataMask), ActiveLanes);
		    flag_eof <= (flag_eof & (lane_data_mask(remainderDataMask) == ActiveLanes));
	      
		    // Give command to send data
		    SendBlock <= 1'b1;
		    if (flag_eof & (lane_data_mask(remainderDataMask) == ActiveLanes))
		      data_fsm_state <= WAIT_TO_SEND_EOF;
		    else if (flag_stored)
		      data_fsm_state <= WAIT_TO_SEND;
		    else if (~FIFO_Empty) begin // if it's possible can fetch new data
		       FIFO_Read <= 1'b1;
		       data_fsm_state <= PREPARE_READ;
		    end else begin 
		       data_fsm_state <= NO_DATA;
		    end		 
		 end
	      end
	   end
	 endcase
      end
   end
   
endmodule // AuroraMultilaneDataFrameFSM

`endif //  `ifndef AURORA_MULTILANE_DATA_FRAME_FSM__SV
