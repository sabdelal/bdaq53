#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Tune global and local threshold and scan for disconnected bump bonds using the threshold/noise shift method.
    Also refer to scan_disconnected_bumps_threshold.py.

    Note:
    The tuning part of this meta scan should be optimally done without high voltage applied (HV = 0V).
'''

from bdaq53.scans.tune_global_threshold import GDACTuning
from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.scans.scan_disconnected_bumps_threshold import BumpConnThrShScan


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    # Start threshold scan at injection setting where a fraction of min_response of the selected
    # pixels see the fraction of hits min_hits
    'min_response': 0.001,
    'min_hits': 0.05,
    # Stop threshold scan at injection setting where a fraction of max_response of the selected
    # pixels see the fraction of hits max_hits
    'max_response': 0.995,
    'max_hits': 0.95,

    'n_injections': 100,

    'VCAL_MED': 200,
    'VCAL_HIGH_start': 200,         # Start value of injection
    'VCAL_HIGH_stop': 4095,         # Maximum injection, can be None
    'VCAL_HIGH_step_fine': 50,      # Step size during threshold scan
    'VCAL_HIGH_step_coarse': 100,    # Step when seaching start

    'bias_voltage_forward': +1,     # Be careful!
    'bias_voltage_reverse': -50,
    'bias_current_limit': 10e-6,

    'maskfile': None,
}

tuning_configuration = {
    'maskfile': None,
    'use_default_chip_configuration': True
}

tuning_configuration_sync = {
    'VCAL_MED': 500,
    'VCAL_HIGH': 1100,

    'chip': {'registers': {'IBIASP1_SYNC': 38,
                           'IBIASP2_SYNC': 10,
                           'IBIAS_SF_SYNC': 100
                           }
             }
}

tuning_configuration_lin_diff = {
    'VCAL_MED': 500,
    'VCAL_HIGH': 1500,

    'chip': {'registers': {'PA_IN_BIAS_LIN': 30,
                           'LDAC_LIN': 50,
                           'PRMP_DIFF': 20,
                           }
             }
}


if __name__ == '__main__':
    tuning_configuration['start_column'] = scan_configuration['start_column']
    tuning_configuration['stop_column'] = scan_configuration['stop_column']
    tuning_configuration['start_row'] = scan_configuration['start_row']
    tuning_configuration['stop_row'] = scan_configuration['stop_row']

    for key in tuning_configuration:
        tuning_configuration_sync[key] = tuning_configuration[key]
        tuning_configuration_lin_diff[key] = tuning_configuration[key]

    if tuning_configuration_sync['start_column'] < 128:
        tuning_configuration_sync['stop_column'] = min(128, tuning_configuration_sync['stop_column'])
        with GDACTuning(scan_config=tuning_configuration_sync) as global_tuning:
            global_tuning.start()
        tuning_configuration_lin_diff['use_default_chip_configuration'] = False

    if tuning_configuration_lin_diff['stop_column'] > 128:
        tuning_configuration_lin_diff['start_column'] = max(128, tuning_configuration_lin_diff['start_column'])
        with GDACTuning(scan_config=tuning_configuration_lin_diff) as global_tuning:
            global_tuning.start()

    tuning_configuration_lin_diff['use_default_chip_configuration'] = False

    # Local tuning seems to have no influence on shift result
    # with TDACTuning(scan_config=tuning_configuration_lin_diff) as local_tuning:
    #     local_tuning.start()

    with BumpConnThrShScan(scan_config=scan_configuration) as disconn_bumps_scan:
        disconn_bumps_scan.start()
