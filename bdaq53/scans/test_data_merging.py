#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script tests the data merging feature of RD53B chips
'''

import tables as tb
import random

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.analysis import rd53b_analysis


class ValueTable(tb.IsDescription):
    register = tb.StringCol(64, pos=0)
    write = tb.UInt16Col(pos=1)
    read = tb.UInt16Col(pos=2)


class TestPatternTable(tb.IsDescription):
    in_pattern = tb.UInt64Col(pos=0)
    out_pattern = tb.UInt64Col(pos=1)


class DataMergingTest(ScanBase):
    scan_id = 'data_merging_test'

    def _configure(self, **_):
        self.chip.write_command(self.chip.CMD_SYNC, repetitions=1000)

    def _scan(self, test_registers='all', test_hitdata='all', invert=True):
        value_table = self.h5_file.create_table(self.h5_file.root, name='values', title='Values', description=ValueTable)
        pattern_table = self.h5_file.create_table(self.h5_file.root, name='pattern_table', title='pattern_table', description=TestPatternTable)

        # Configure BDAQ board for data merging
        self.bdaq.dm_invert(invert)  # Invert the DM signal (because of the Mini-DP cable) TODO: Invert in FW?
        self.bdaq['FIFO'].get_data()
        self.bdaq.dm_output_enable('all')
        self.chip.enable_data_merging()

        # Test 1: Hit data
        self.log.info('Testing data merging: Hit data. Testing {} hit data words'.format(test_hitdata))
        self.bdaq.dm_type('hitdata')
        self.bdaq.set_monitor_filter(mode='block')  # Block monitor frames
        self.bdaq['FIFO'].get_data()

        for _ in range(10) if test_hitdata == 'all' else test_hitdata:
            rx_data = 0
            row = pattern_table.row
            row['in_pattern'] = 0x12345678
            self.bdaq.dm_send_hitdata(data=row['in_pattern'])
            timeout = 100
            for _ in range(timeout):
                self.chip.write_command(self.chip.CMD_SYNC, repetitions=100, wait_for_done='true')
                if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                    data = self.bdaq['FIFO'].get_data()
                    for elem in data:
                        if not elem & 0x80010000:
                            rx_data = rx_data << 16
                            rx_data += elem
                    if len(data) == 0:
                        continue
            row['out_pattern'] = rx_data

        # Test 2: Register data
        self.log.info('Starting data merging test. Testing {} registers with random data'.format(test_registers))
        self.bdaq.dm_type('monitor')
        self.bdaq.set_monitor_filter(mode='filter')  # Allow monitor frames to pass

        for dm_addr in range(256) if test_registers == 'all' else test_registers:
            dm_data = random.randint(0, int(2**16 - 1))
            row = value_table.row
            row['register'] = dm_addr
            row['write'] = dm_data
            row['read'] = 0

            for BTF in [0x99]:  # d2:MM, 99:MA, 55:AM, AA:B4
                self.bdaq.dm_send_register(header=BTF, address=dm_addr, data=dm_data)
                if self.bdaq.board_version != 'SIMULATION':
                    self.chip.write_command(self.chip.CMD_SYNC, repetitions=100, wait_for_done='true')

                timeout = 100
                for _ in range(timeout):
                    if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                        data = self.bdaq['FIFO'].get_data()
                        interpreted_userk_data = rd53b_analysis.interpret_userk_data(data)
                        processed_userk_data = analysis_utils.process_userk(interpreted_userk_data)
                        if len(processed_userk_data) == 0:
                            continue
                        for val in processed_userk_data:
                            self.log.debug('Reading data merging data. Address= {}, Data= {})'.format(hex(val['Address']), hex(val['Data'])))
                            row['read'] = val['Data']
                            row.append()

                    if self.bdaq.board_version != 'SIMULATION':
                        self.chip.write_command(self.chip.CMD_SYNC, repetitions=10, wait_for_done='true')

        self.chip.disable_data_merging()
        self.log.success('Scan finished')

    def _analyze(self):
        result = True
        self.log.info('Comparing data...')
        with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
            data = in_file.root.values[:]
            pattern_table = in_file.root.pattern_table[:]
            if len(data) == 0:
                self.log.error('Regitser test failed! No register value received.')

        for res in data:
            if res[2] != res[1]:
                self.log.error('Register read back a wrong value! Expected {}\treceived {}'.format(res[1], res[2]))
                result = False
        for res in pattern_table:
            if res[0] != res[1]:
                self.log.error('Register read back a wrong value! Expected {}\treceived {}'.format(res[0], res[1]))
                result = False
        if result is True:
            self.log.success('Successfully tested {} registers.'.format(len(data)))
        return result


if __name__ == '__main__':
    with DataMergingTest() as test:
        test.start()
