#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.
'''

import tables as tb
from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import plotting


scan_configuration = {
    'periphery_device': 'Multimeter',
    'DAC': 'VCAL_HIGH',
    'value_start': 0,
    'value_stop': 4096,
    'value_step': 100
}


class DacTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=0)
    dac = tb.UInt32Col(pos=1)
    adc = tb.UInt32Col(pos=2)
    voltage = tb.Float32Col(pos=3)


class DACScan(ScanBase):
    scan_id = 'dac_linearity_scan'

    def _configure(self, **_):
        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

    def _scan(self, DAC='VCAL_HIGH', value_start=0, value_stop=4096, value_step=100, periphery_device='Multimeter', **_):
        '''
        DAC linearity scan main loop

        Parameters
        ----------
        DAC : str
            Name of the DAC to scan
        value_start : int
            First value for this DAC
        value_stop : int
            Last value of this DAC, exclusive
        value_step : int
            Steps between DAC values
        '''

        value_range = range(value_start, value_stop, value_step)
        self.configuration['scan']['type'] = 'I' if DAC in self.chip.current_mux.keys() else 'U'

        input('Connect the multimeter to the correct pin for chip {0} and press Enter to continue...'.format(self.chip.get_sn()))

        rows = []
        for scan_param_id, value in tqdm(enumerate(value_range), total=len(value_range), unit=' Values'):
            with self.readout(scan_param_id=scan_param_id):
                self.chip.registers[DAC].write(value)
                adc_value = self.chip.get_ADC_value(DAC, measure='v')[0]
                voltage = self.periphery.aux_devices[periphery_device].get_voltage()
                rows.append((value, adc_value, voltage, scan_param_id))

        self.data.dac_data_table = self.h5_file.create_table(self.h5_file.root, name='dac_data',
                                                             title='dac_data', description=DacTable)
        row = self.data.dac_data_table.row
        for value, adc_value, voltage, scan_param_id in rows:
            row['dac'] = value
            row['adc'] = adc_value
            row['voltage'] = voltage
            row['scan_param_id'] = scan_param_id
            row.append()
            self.data.dac_data_table.flush()

        self.log.success('Scan finished')

    def _analyze(self):
        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=self.output_filename + '.h5') as p:
                p.create_standard_plots()


if __name__ == "__main__":
    with DACScan(scan_config=scan_configuration) as scan:
        scan.start()
