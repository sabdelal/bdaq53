#!/usr/bin/env python
''' Entry point to simplify the usage from command line for
the online_monitor with bdaq53 plugins. Not really needed
start_online_monitor config.yaml would also work...
'''
import argparse
import sys
import os
import subprocess
import psutil
import logging

import yaml

from online_monitor.OnlineMonitor import OnlineMonitorApplication
from online_monitor.utils import utils, producer_sim_manager


def run_script_in_shell(script, arguments, command=None):
    if os.name == 'nt':
        creationflags = subprocess.CREATE_NEW_PROCESS_GROUP
    else:
        creationflags = 0
    return subprocess.Popen("%s %s %s" % ('python' if not command else command, script, arguments),
                            shell=True, creationflags=creationflags)


def kill(proc):
    ''' Kill process by id, including subprocesses; works for linux and windows '''
    process = psutil.Process(proc.pid)
    for child_proc in process.children(recursive=True):
        child_proc.kill()
    process.kill()


def replay_data(data_file, delay):
    ''' Replay a raw data file using the bdaq53 daq simulation producer '''

    if not os.path.isabs(data_file):
        data_file = os.path.join(sys.argv[0], data_file)
    if not os.path.isfile(data_file):
        logging.error('Cannot open %s for replay!', data_file)
        return

    # Create config yaml
    conf = {}
    # Add producer
    devices = {}
    devices['DAQ0'] = {'backend': 'tcp://127.0.0.1:5500',
                       'kind': 'bdaq53_sim',
                       'delay': delay,
                       'data_file': data_file
                       }
    conf['producer_sim'] = devices
    with open('tmp_cfg.yml', 'w') as outfile:
        outfile.write(yaml.dump(conf, default_flow_style=False))

    producer_sim_manager.ProducerSimManager('tmp_cfg.yml')

#     prod_sim_proc = run_script_in_shell('', 'tmp_cfg.yml', 'start_producer_sim')
#     while(True):
#         try:
#             time.sleep(2)
#         except KeyboardInterrupt:
#             kill(prod_sim_proc)


def main():
    # Parse program arguments
    description = "Start online monitor for bdaq53"
    parser = argparse.ArgumentParser(prog='bdaq53_monitor',
                                     description=description,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--replay', type=str,
                        help='Absolute path to raw data file to replay')
    parser.add_argument('--delay', type=float,
                        help='Additional delay when replaying data in seconds')
    parser.add_argument('-f', '--config_file',
                        type=str,
                        nargs='?',
                        help='Path to online monitor configuration file. If not defined, the default configuration is used in:\n'
                        'bdaq53/analysis/online_monitor/bdaq53_monitor.yaml',
                        metavar='config_file')
    args = parser.parse_args()

    if not args.config_file:
        folder = os.path.dirname(os.path.realpath(__file__))
        args.config_file = os.path.join(folder, r'bdaq53_monitor.yaml')
    utils.setup_logging('INFO')

    if args.replay:
        data_file = args.replay
        if not os.path.isabs(data_file):
            data_file = os.path.join(sys.argv[0], data_file)
        if not os.path.isfile(data_file):
            logging.error('Cannot open %s for replay!', data_file)
            return
        # Add simulation to configuiration
        conf = utils.parse_config_file(args.config_file, expect_receiver=True)
        with open('tmp_cfg.yml', 'w') as outfile:
            # Add producer
            devices = {}
            devices['DAQ0'] = {'backend': 'tcp://127.0.0.1:5500',
                               'kind': 'bdaq53_sim',
                               'delay': args.delay if args.delay else 0,
                               'data_file': data_file
                               }
            conf['producer_sim'] = devices
            outfile.write(yaml.dump(conf, default_flow_style=False))
            args.config_file = 'tmp_cfg.yml'

    # Start the converter
    run_script_in_shell('', args.config_file, 'start_online_monitor')


if __name__ == '__main__':
    main()
