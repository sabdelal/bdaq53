#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    BDAQ53 class for creating combined multi-chip module files from existing scan results.

    Standalone usage:
        At the bottom of the script, enter
        - the type of the module
        - the run_name (filename w/o extension) of the interpreted scan results
        - the path to the module's output directory
        - the module's chips' serial numbers
        and run the script.
'''

import os
import numpy as np
import tables as tb
import math
from functools import reduce
from tqdm import tqdm
from itertools import islice
import warnings

from bdaq53.system import logger
from bdaq53.analysis import analysis_utils as au
from bdaq53.modules.module_type import ModuleType


def _load_tables(write_successive=False):
    ''' Decorator that opens all nodes from self.in_files
        If write_successive is False:
            All data is loaded and passed to decorated function
            returned data gets written into self.out_file
        If write_successive is True:
            Only nodes get passed to decorated function
            also passes arguments for creating a out node
            But data can be loaded successively by the decorated function
    '''
    def decorator(func):
        def wrapper(self, in_node, table_name, out_node=None, out_name=None, out_title=None, **kwargs):
            in_tables0 = [_get_node(in_file, in_node, table_name) for in_file in self.in_files]
            # Sort out chips which do not have specified node
            sorted_out = [(table, self.chip_ids[i]) for i, table in enumerate(in_tables0) if table is not None]

            # Only failes if no chip has specified node
            if len(sorted_out) == 0:
                self.log.warning('Node %s %s not found in %s.', in_node, table_name, self.in_files[0].title)
                return False

            in_tables, chip_ids = zip(*sorted_out)

            # Arguments of node for self.out_file
            args = {
                'where': in_node if out_node is None else out_node,
                'name': table_name if out_name is None else out_name,
                'title': in_tables[0].title if out_title is None else out_title,
                'filters': in_tables[0].filters
            }

            if write_successive:
                # Just pass nodes and arguments
                func(self, in_tables, chip_ids, args, **kwargs)
            else:
                # Load all data
                data = [table[:] for table in in_tables]
                # Sort out empty data
                non_empty = [(d, i) for d, i in zip(data, chip_ids) if len(d) > 0]
                if len(non_empty) > 0:
                    data, chip_ids = zip(*non_empty)
                    # Combine Data
                    args['obj'] = func(self, list(data), chip_ids, **kwargs)
                else:
                    args['obj'] = data[0]

                # Write out combined data
                if type(in_tables[0]) == tb.table.Table:
                    self.out_file.create_table(**args)
                elif type(in_tables[0]) == tb.carray.CArray:
                    self.out_file.create_carray(**args)

            # Note which node was covered
            self._covered_nodes.add(in_tables[0]._v_pathname)
            return True
        return wrapper
    return decorator


def _combine_dicts(func):
    ''' Decorator which turns a list of structured arrays into a list of dictionaries
        and turns the return value from a dictionary into a structured array
    '''
    def wrapper(self, data, chip_ids, **kwargs):
        dicts = [au.ConfigDict(table) for table in data]
        result = func(self, dicts, chip_ids, **kwargs)
        return np.array([(key, str(result[key])) for key in result.keys()], dtype=data[0].dtype)
    return wrapper


def _get_node(file, node_str, table_name=None):
    ''' If existing, returns the node of file given by node_str
        If defined returns node (table_name) at parent node (node_str)
    '''
    try:
        node = reduce(getattr, [file.root] + list(filter(None, node_str.split('/'))))
        if table_name is not None:
            node = getattr(node, table_name)
        return node
    except tb.NoSuchNodeError:
        return None


class CombineModuleFiles(object):
    ''' Class to combine interpreted data files of multiple chips of a multi-chip module '''

    def __init__(self, analyzed_data_files, chip_ids=None, module_type=None, chip_sns=None, target_file=None):
        ''' Parameters:
                analyzed_data_files : list of str
                    list of filenames including .h5 ending
                chip_ids : list of int
                    chip_ids of analyzed_data_files. If None: obtained from configuration_ins
                module_type : str
                    module type, defined in modules/module_types.yaml. If None: obtained from configuration_in of first file
                target_file : str
                    target file name. If None: basename at common path of analyzed_data_files
        '''
        self.log = logger.setup_derived_logger('CombineModuleFile')

        self.analyzed_data_files = [filename for filename in analyzed_data_files if filename]
        self.chip_ids = chip_ids
        self._module_type = module_type
        self._chip_sns = chip_sns

        if target_file is None:
            if len(self.analyzed_data_files) > 1:
                path = os.path.commonpath(self.analyzed_data_files)
            else:
                path = os.path.join(os.path.dirname(self.analyzed_data_files[0]), os.pardir)
            name = os.path.basename(self.analyzed_data_files[0])
            self.target_file = os.path.join(path, name)
        else:
            self.target_file = target_file

        self.scan_params_transformation = None
        self.rebin_columm = None

    def open_files(self):
        ''' Opens all module files, sets up class attributes and creates output_file '''

        try:
            self.in_files = [tb.open_file(in_file, 'r') for in_file in self.analyzed_data_files]
        except IOError as e:
            self.log.error(e)
            raise RuntimeError('Interpreted data file does not exist!')

        self.chip_configs = [au.ConfigDict(in_file.root.configuration_in.chip.settings[:]) for in_file in self.in_files]
        if self.chip_ids is None:
            self.chip_ids = [conf['chip_id'] for conf in self.chip_configs]

        if len(set(self.chip_ids)) < len(self.chip_ids):
            raise ValueError('Multiple chip files with the same chip_id where supplied.')

        self.module_config_replacement = {}
        self.module_config = au.ConfigDict(self.in_files[0].root.configuration_in.chip.module[:])
        if self._module_type is None:
            try:
                self._module_type = self.module_config['module_type']
            except KeyError:
                raise ValueError("Module configuration node has no key 'module_type'. Maybe this is an older file?")
        else:
            self.module_config_replacement['module_type'] = self._module_type

        self.module_type = ModuleType(self._module_type, self.chip_configs[0]['chip_type'])

        if len(set(self.chip_ids).difference(set(self.module_type.get_chip_ids()))) > 0:
            raise ValueError('Some chips are not supported by the module type.')

        self.config_node_names = [node for node in ('/configuration_out', '/configuration_in') if node in self.in_files[0]]
        self.config_nodes = [getattr(in_file.root, self.config_node_names[0][1:]) for in_file in self.in_files]

        self.run_configs = [au.ConfigDict(config.scan.run_config[:]) for config in self.config_nodes]
        self.chip_sns = self._chip_sns if self._chip_sns else [config['chip_sn'] for config in self.run_configs]
        self.chip_nodes = self.chip_sns
        self.scan_id = self.run_configs[0]['scan_id']
        self.chip_type = self.run_configs[0]['chip_type']
        self.analysis_config = au.ConfigDict(self.config_nodes[0].bench.analysis[:])

        # create output file
        self.out_file = tb.open_file(self.target_file, 'w', self.in_files[0].title)

        # Keep track of nodes that have to be combined
        self._nodes_to_cover = set(node._v_pathname for node in self.in_files[0] if type(node) is not tb.group.Group)
        self._covered_nodes = set('/')

    def close_files(self):
        ''' Checks if all nodes of input files where covered and closes all opened files. '''

        missed_nodes = self._nodes_to_cover.difference(self._covered_nodes)

        if len(missed_nodes) > 0:
            self.log.warning('Following nodes of the imput files where not covered by this script: {}'.format(missed_nodes))
        else:
            self.log.success('Module files successfully merged: {}'.format(self.target_file))

        try:
            self.out_file.close()
            for in_file in self.in_files:
                in_file.close()
        except Exception:
            pass

    def combine_standard_tables(self, combine_hit_data=False):
        ''' Combines all tables used by standard scans'''

        self.log.info('Combining standard tables into a module file...')

        if self.scan_id in ['autorange_threshold_scan', 'bump_connection_bias_thr_shift_scan']:
            self.rebin_columm = 'vcal_high'

        for config_i, config in enumerate(self.config_node_names):

            self.out_file.create_group('/', config[1:])
            self.out_file.create_group(config, 'scan')
            self.out_file.create_group(config, 'bench')
            self.out_file.create_group(config, 'module')
            self.out_file.create_group(config + '/scan', 'chips')
            self.out_file.create_group(config + '/module', 'masks')
            self.out_file.create_group(config + '/module', 'chips')
            with warnings.catch_warnings():
                warnings.simplefilter('ignore', tb.NaturalNameWarning)
                for chip_node in self.chip_nodes:
                    self.out_file.create_group(config + '/scan/chips', chip_node)
                    self.out_file.create_group(config + '/module/chips', chip_node)

            scan_node = config + '/scan'
            self._merge_dicts(scan_node, 'run_config')
            self._merge_dicts(scan_node, 'scan_config')
            self._copy_nodes_individually(scan_node, 'scan_params', scan_node + '/chips')
            self._merge_scan_params(scan_node, 'scan_params', set_scan_params_transformation=(config_i == 0))
            self._write_scan_enabled_mask(config)

            bench_node = config + '/bench'
            self._merge_dicts(bench_node, 'general')
            self._merge_dicts(bench_node, 'periphery')
            self._merge_dicts(bench_node, 'analysis')
            self._merge_dicts(bench_node, 'TLU')
            self._merge_dicts(bench_node, 'TDC')
            self._merge_dicts(bench_node, 'notifications')
            self._merge_dicts(bench_node, 'calibration')
            self._merge_dicts(bench_node, 'hardware')

            chip_node = config + '/chip'
            chip_masks_node = config + '/chip/masks'
            module_node = config + '/module'
            module_masks_node = config + '/module/masks'
            chips_node = config + '/module/chips'
            self._merge_dicts(chip_node, 'module', module_node, replace=self.module_config_replacement)
            self._merge_dicts(chip_node, 'registers', module_node)
            self._mean_dicts(chip_node, 'calibration', module_node)
            self._copy_nodes_individually(chip_node, 'trim', chips_node)
            self._copy_nodes_individually(chip_node, 'settings', chips_node)
            self._copy_nodes_individually(chip_node, 'registers', chips_node)
            self._copy_nodes_individually(chip_node, 'calibration', chips_node)
            self._concatenate_maps(chip_node, 'use_pixel', module_node)
            self._concatenate_maps(chip_masks_node, 'hitbus', module_masks_node)
            self._concatenate_maps(chip_masks_node, 'tdac', module_masks_node)
            self._concatenate_maps(chip_masks_node, 'injection_delay', module_masks_node)
            self._concatenate_maps(chip_masks_node, 'lin_gain_sel', module_masks_node)
            self._concatenate_maps(chip_masks_node, 'injection', module_masks_node)
            self._concatenate_maps(chip_masks_node, 'enable', module_masks_node)
            self._write_chip_id_map(config)

        if self.scan_id not in ['dac_linearity_scan', 'sensor_iv_scan', 'seu_test']:
            self._concatenate_maps('/', 'HistOcc', transform_map=True)
            self._concatenate_maps('/', 'HistTot', transform_map=True)
            self._concatenate_maps('/', 'HistRelBCID', transform_map=True)
            self._concatenate_maps('/', 'HistTrigID', transform_map=True)
            self._add_hists('/', 'HistEventStatus')
            self._add_hists('/', 'HistBCIDError')

        if self.scan_id in ['threshold_scan', 'global_threshold_tuning', 'in_time_threshold_scan', 'fast_threshold_scan', 'autorange_threshold_scan', 'crosstalk_scan']:
            self._concatenate_maps('/', 'ThresholdMap')
            self._concatenate_maps('/', 'NoiseMap')
            self._concatenate_maps('/', 'Chi2Map')

        if self.scan_id in ['tot_calibration']:
            self._concatenate_maps('/', 'HistTotCal')
            self._concatenate_maps('/', 'HistTotCalMean')
            self._concatenate_maps('/', 'HistTotCalStd')
            self._concatenate_maps('/', 'TOThist')

        if self.scan_id in ['hitor_calibration']:
            self._add_hists('/', 'hist_2d_tdc_vcal')
            self._concatenate_maps('/', 'hist_tot_mean', transform_map=True)
            self._concatenate_maps('/', 'hist_tot_std', transform_map=True)
            self._concatenate_maps('/', 'hist_tdc_mean', transform_map=True)
            self._concatenate_maps('/', 'hist_tdc_std', transform_map=True)
            self._concatenate_maps('/', 'lookup_table')

        if self.scan_id in ['dac_linearity_scan']:
            self.log.warning('dac_linearity_scan is not supported.')

        if self.scan_id in ['bump_connection_source_scan', 'bump_connection_crosstalk_scan', 'bump_connection_bias_thr_shift_scan', 'bump_connection_analysis_of_source_scan']:
            self._concatenate_maps('/', 'BumpConnectivityMap')
            self._add_dicts('/', 'BumpConnectivityStatistics')

        if self.scan_id in ['bump_connection_bias_thr_shift_scan']:
            self._concatenate_maps('/', 'ThresholdMapReverse')
            self._concatenate_maps('/', 'ThresholdMapForward')
            self._concatenate_maps('/', 'ThresholdShiftMap')
            self._concatenate_maps('/', 'NoiseMapReverse')
            self._concatenate_maps('/', 'NoiseMapForward')
            self._concatenate_maps('/', 'NoiseShiftMap')
            self._concatenate_maps('/', 'Chi2MapReverse')
            self._concatenate_maps('/', 'Chi2MapForward')
            self._mean_dicts('/', 'Cuts')

        if self.analysis_config['analyze_tdc']:
            self._add_hists('/', 'HistTdcStatus')

        if self.analysis_config['analyze_ptot']:
            self._concatenate_maps('/', 'HistPToT', transform_map=True)
            self._concatenate_maps('/', 'HistPToA')

        if self.analysis_config['store_hits'] and combine_hit_data:
            self._merge_lists_succesive('/', 'Hits', sort_col='event_number')
        else:
            self._covered_nodes.add('/Hits')

        if self.analysis_config['cluster_hits']:
            self._add_hists('/', 'HistClusterSize')
            self._add_hists('/', 'HistClusterTot')
            self._add_hists('/', 'HistClusterShape')
            if combine_hit_data:
                self._merge_lists_succesive('/', 'Cluster', sort_col='event_number')
            else:
                self._covered_nodes.add('/Cluster')

    # Internal functions

    def _copy_nodes_individually(self, in_node, table_name, out_node):
        ''' Opens all tables at in_node and writes them in indiviual nodes
            with names self.chip_nodes and parent node out_node
        '''
        for chip_node, in_file in zip(self.chip_nodes, self.in_files):
            in_table = getattr(_get_node(in_file, in_node), table_name)
            target_node = getattr(_get_node(self.out_file, out_node), chip_node)
            in_table.copy(target_node, table_name)
            self._covered_nodes.add(in_table._v_pathname)

    def _write_scan_enabled_mask(self, config_node):
        ''' Creates a map specifying all regions which where enabled by the scan '''
        masks = [None] * len(self.in_files)
        for i, in_file in enumerate(self.in_files):
            config = _get_node(in_file, config_node)
            use_pixel = config.chip.use_pixel
            scan_config = au.ConfigDict(config.scan.scan_config[:])
            masks[i] = np.ones(shape=use_pixel.shape, dtype=use_pixel.dtype)
            masks[i][:scan_config['start_column'], :] = False
            masks[i][scan_config['stop_column']:, :] = False
            masks[i][:, :scan_config['start_row']] = False
            masks[i][:, scan_config['stop_row']:] = False
        combined_mask = self._concat_maps(masks, self.chip_ids)
        self.out_file.create_carray(config_node + '/scan', name='enable', title='Enabled scan range', obj=combined_mask, filters=use_pixel.filters)

    def _write_chip_id_map(self, config_node):
        ''' Creates a map of the physical positions of all chip_ids '''
        use_pixel = _get_node(self.in_files[0], config_node).chip.use_pixel
        chip_ids = self.module_type.get_chip_ids()
        masks = [np.full(use_pixel.shape, chip_id) for chip_id in chip_ids]
        combined_mask = self._concat_maps(masks, chip_ids)
        self.out_file.create_carray(config_node + '/module', name='chip_id', title='Chip id map', obj=combined_mask, filters=use_pixel.filters)

    @_load_tables()
    def _add_hists(self, hists, chip_ids):
        ''' Adds all histograms allong axis 0 '''
        return np.sum(hists, axis=0)

    @_load_tables()
    def _concatenate_maps(self, maps, chip_ids, **kwargs):
        ''' Write concatenated maps '''
        return self._concat_maps(maps, chip_ids, **kwargs)

    def _concat_maps(self, maps, chip_ids, transform_map=False):
        ''' Concatenate maps according to module definition.
            If entries need to be rebinned (modified scan paramters) then self.scan_params_transformation must be set:
                list of (chip_id, transformation_matrix) pairs
        '''

        if transform_map and self.scan_params_transformation:
            for i, (m_id, m) in enumerate(zip(chip_ids, maps)):
                for t_id, t in self.scan_params_transformation:
                    if m_id == t_id:
                        new_shape = list(maps[i].shape)
                        new_shape[2] = t.shape[0]
                        new_data = np.zeros_like(maps[i], shape=new_shape)
                        for x, row in enumerate(m):
                            for y, data in enumerate(row):
                                new_data[x, y] = t.dot(data)
                        maps[i] = new_data
                        break

        maps = [[self._get_map(maps, chip_ids, sel_id) for sel_id in row] for row in self.module_type.chip_id_map]
        combined = self.module_type.concatenate_maps(maps)
        return combined

    def _get_map(self, maps, chip_ids, selection, fill_value=0):
        ''' Returns map of (map, chip_id) pairs, where chip_id == selection.
            If not supplied, a default map is returned
            if specified in module definition, map is extended along axis 1 to double size
        '''
        return next((m for m, chip_id in zip(maps, chip_ids) if chip_id == selection), np.full_like(maps[0], fill_value))

    @_load_tables()
    @_combine_dicts
    def _merge_dicts(self, dicts, chip_ids, replace={}, custom_keys=None):
        ''' Merges dictionaries into one while considering conflicting values.
            Keys get extended naming scheme for conflicting entries or if its value should be replaced by the value supplied in replace.
        '''
        if custom_keys is None:
            custom_keys = self._custom_keys
        keys = list({key: None for dic in dicts for key in dic.keys()}.keys())
        out_dict = {}
        for key in keys:
            values, chip_id = zip(*[(dic[key], i) for dic, i in zip(dicts, chip_ids) if key in dic])
            # if all values are equal
            if values.count(values[0]) == len(values) and key not in replace:
                out_dict[key] = values[0]
            # if values are deviating use extended naming scheme
            else:
                for i in range(len(values)):
                    out_dict[custom_keys(key, chip_id[i])] = values[i]
        for key in replace:
            out_dict[key] = replace[key]
        return out_dict

    def _custom_keys(self, key, chip_id, suffix="_chip_%i"):
        ''' Adds chip specific suffix to key to resolve conflicting entries '''
        return key + suffix % chip_id

    @_load_tables()
    @_combine_dicts
    def _add_dicts(self, dicts, chip_ids):
        ''' Combines dictionaries while adding values of entries whith same key '''
        combined = {key: 0 for dic in dicts for key in dic.keys()}
        for dic in dicts:
            for key in dic:
                combined[key] += dic[key]
        return combined

    @_load_tables()
    @_combine_dicts
    def _mean_dicts(self, dicts, chip_ids):
        ''' Combines dictionaries while using mean of values of entries whith same key '''
        keys = list({key: None for dic in dicts for key in dic.keys()}.keys())
        return {key: np.mean([dic[key] for dic in dicts]) for key in keys}

    @_load_tables()
    def _merge_scan_params(self, lists, chip_ids, set_scan_params_transformation=False):
        ''' Merges the supplied scan parameters:
                - If all are identical, return first
                - If not identical:
                    - For each group of scan paramters which are equal except in self.rebin_columm:
                        A new paramter range is generated starting from the lowest value and using the difference of the first two values as the step.
                        New ranges get concatenated into new table with new scan_params_ids.
                    - If set_scan_params_transformation is set:
                        self.scan_params_transformation is calculated which is used to rebin maps which depend and the scan parameters.
        '''
        if len({lis.size for lis in lists}) == 1 and all(all(x == y for x, y in zip(lis, lists[0])) for lis in lists[1:]):
            return lists[0]

        dtype0 = lists[0].dtype
        dtype = [(x, str(y[0])) for x, y in dtype0.fields.items()]

        if self.rebin_columm is None:
            raise ValueError('No rebin column was supplied to resolve merge conflicts of the scan parameters.')

        settings = {tuple((par, row[par]) for par in lis.dtype.names
                          if par not in (self.rebin_columm, 'scan_param_id')): None
                    for lis in lists for row in lis}.keys()
        old_ranges = [[row[self.rebin_columm] for lis in lists for row in lis
                       if all(row[sk] == sv for sk, sv in setting)]
                      for setting in settings]

        new_ranges = [None] * len(old_ranges)
        for i, parameter_range in enumerate(old_ranges):
            step = abs(parameter_range[1] - parameter_range[0])
            new_ranges[i] = np.arange(min(parameter_range), max(parameter_range) + step, step)
        new_range_starts = [sum(len(r) for r in new_ranges[:i]) for i in range(len(new_ranges))]
        new_len = sum(len(r) for r in new_ranges)

        if set_scan_params_transformation:
            self.scan_params_transformation = [None] * len(chip_ids)
            for j, (lis, chip_id) in enumerate(zip(lists, chip_ids)):
                old_len = len(lis)
                ar = np.zeros((new_len, old_len))
                for s, new_range, start in zip(settings, new_ranges, new_range_starts):
                    selected = [(i, row[self.rebin_columm]) for i, row in enumerate(lis) if all(row[sk] == sv for sk, sv in s)]
                    for col, par in selected:
                        if par < new_range[0]:
                            ar[start, col] = 1
                        elif par >= new_range[-1]:
                            ar[start + len(new_range) - 1, col] = 1
                        else:
                            for i in range(len(new_range) - 1):
                                if new_range[i + 1] > par and par >= new_range[i]:
                                    r = (par - new_range[i]) / (new_range[i + 1] - new_range[i])
                                    ar[start + i, col] = 1 - r
                                    ar[start + i + 1, col] = r
                                    break
                self.scan_params_transformation[j] = (chip_id, ar)

        self.scan_params_transformation

        combined_data = np.array([], dtype=dtype)

        for setting, new_range in zip(settings, new_ranges):
            range_len = len(new_range)
            new_rows = np.zeros(range_len, dtype=dtype)
            for key, value in setting:
                new_rows[key] = value
            new_rows['scan_param_id'] = list(range(len(combined_data), len(combined_data) + range_len))
            new_rows[self.rebin_columm] = new_range
            combined_data = np.concatenate((combined_data, new_rows))

        return combined_data

    @_load_tables(write_successive=True)
    def _merge_lists_succesive(self, in_tables, chip_ids, table_args, step_size=100000, sort_col=None):
        ''' Merge lists succesively by iterating over all rows,
            If list length is bigger then step_size, a progress bar is shown.
            When a sort column is specified, the rows are sorted along this column.
        '''
        dtype0 = tb.dtype_from_descr(next((table for table in in_tables if table.nrows > 0), in_tables[0]).description)
        dtype = [(x, str(y[0])) for x, y in dtype0.fields.items()]
        dtype += [('chip_id', 'u1')]
        empty_data = np.array([], dtype=dtype)
        n_rows = sum([table.nrows for table in in_tables])

        out_table = self.out_file.create_table(**table_args, obj=empty_data, expectedrows=n_rows)
        out_row = out_table.row

        pbar = tqdm(total=n_rows, desc='Merge ' + table_args['name'], unit=' rows') if n_rows > step_size else None

        if sort_col is None:
            iterator = self._iterate_rows_sequential(in_tables, chip_ids, step_size, pbar)
        else:
            iterator = self._iterate_rows_sorted(in_tables, chip_ids, step_size, pbar, n_rows, sort_col)

        keys = dtype0.fields.keys()

        for row, chip_id in iterator:
            for key in keys:
                out_row[key] = row[key]
            out_row['chip_id'] = chip_id
            out_row.append()
        out_table.flush()

        if pbar is not None:
            pbar.n = n_rows
            pbar.refresh()
            pbar.close()

    def _iterate_rows_sequential(self, tables, chip_ids, step_size, pbar):
        ''' Iterates over all rows one table after another.
            Updates progress bar after step_size rows.
        '''
        for table, chip_id in zip(tables, chip_ids):
            if table.nrows == 0:
                break
            it = table.iterrows()
            for _ in range(math.ceil(table.nrows / step_size)):
                rows = islice(it, step_size)
                for row in rows:
                    yield row, chip_id
                if pbar is not None:
                    pbar.update(step_size)

    def _iterate_rows_sorted(self, tables, chip_ids, step_size, pbar, n_rows, sort_col):
        ''' Iterates over all rows while zipping them: Iteratively choose next row of the table where sort_col is the lowest
            Updates progress bar after step_size rows.
        '''
        cur_tables = [table for table in tables if table.nrows > 0]
        chip_ids = [chip_id for table, chip_id in zip(tables, chip_ids) if table.nrows > 0]
        its = [table.iterrows() for table in cur_tables]
        rows = [next(it) for it in its]
        next_keys = [row[sort_col] for row in rows]
        for _ in range(math.ceil(n_rows / step_size)):
            for _ in range(step_size):
                min_i = next_keys.index(min(next_keys))
                yield rows[min_i], chip_ids[min_i]
                try:
                    next_row = next(its[min_i])
                    rows[min_i] = next_row
                    next_keys[min_i] = next_row[sort_col]
                except StopIteration:
                    if len(cur_tables) == 1:
                        break
                    for lis in (cur_tables, its, rows, next_keys):
                        lis = [x for i, x in enumerate(lis) if i != min_i]
            if pbar is not None:
                pbar.update(step_size)

    def __enter__(self):
        self.open_files()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close_files()


if __name__ == '__main__':

    # Filename of interpreted data files of same module without path and extension (ScanBase.run_name)
    interpreted_data_filename = '20200101_120000_digital_scan_interpreted'

    # Common module directory with output of all chips in subdirectories
    module_path = '...output_data/module_0'

    # Relative paths to chip directories
    chip_rel_paths = ['0x0001', '0x0002']

    # Optional parameters that replace default values:
    module_type = None  # (str) module type defined in modules/module_types.yaml.
    new_chip_ids = None  # (list of int) chip_ids of analyzed_data_files
    new_chip_sns = None  # (list of int) chip_sns of analyzed_data_files
    target_file = None  # (str) target file name

    filenames = [os.path.join(module_path, rel_path, interpreted_data_filename + '.h5') for rel_path in chip_rel_paths]

    with CombineModuleFiles(analyzed_data_files=filenames,
                            chip_ids=new_chip_ids,
                            module_type=module_type,
                            chip_sns=new_chip_sns,
                            target_file=target_file) as cmf:
        cmf.combine_standard_tables()
