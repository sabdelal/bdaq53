#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import logging

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

from bdaq53.tests import utils

configuration = {
    'n_injections': 2,

    'mask_step': 1,
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 384,

    'bench': {'general': {'use_database': False}}
}


class SelfTriggerDigitalScan(ScanBase):
    scan_id = 'self_trigger_digital_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=384, **_):
        '''_bdaq
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        self.configuration['bench']['general']['abort_on_rx_error'] = False  # simulation too slow for RX sync check
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        # self.chip.masks['hitbus'][start_column:stop_column, start_row:stop_row] = True

        # self.chip.masks.update_broadcast(force=True, write_TDAC=False)
        self.chip.enable_core_col_clock(range(0, 50))
        self.chip.enable_macro_col_cal(range(0, 50))
        self.chip.setup_digital_injection()
        indata = self.chip.registers['TriggerConfig'].get_write_command(0b0111110100)
        indata += self.chip.registers['SelfTriggerConfig_1'].get_write_command(0b110001)
        indata += self.chip.registers['SelfTriggerConfig_0'].get_write_command(0b011110100000100)
        indata += self.chip.registers['HitOrPatternLUT'].get_write_command(0xFFFE)
        self.chip.write_command(indata)

    def _scan(self, n_injections=3, **_):
        '''
        Digital scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.
        '''

        self.log.info('Starting scan...')

        with self.readout():
            for _ in range(n_injections):
                indata = self.chip.write_sync(write=False) * 10
                indata += self.chip.registers['CalibrationConfig'].get_write_command(int('0b10' + format(9, '06b'), 2))  # Enable digital injection with zero delay
                indata += self.chip.write_cal(cal_edge_mode=1, cal_edge_width=4, cal_edge_dly=2, write=False)  # Injection
                indata += self.chip.write_sync(write=False) * 500  # Wait for latency
                self.chip.write_command(indata, repetitions=1)

    def _analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=True) as a:
            a.analyze_data()

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


class TestDigitalScan(unittest.TestCase):
    def test_scan_digital(self):
        logging.info('Starting self trigger digital scan test')

        self.scan = SelfTriggerDigitalScan(bdaq_conf=utils.setup_cocotb(test_dc=0, chip='ITkPixV1', rx_lanes=1), scan_config=configuration)
        self.scan.scan()
        self.scan.analyze()
        self.scan.close()

    def tearDown(self):
        utils.close_sim()
        # shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
