#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Perform ThresholdScans with decreasing amount of wait_cycles in between injections
    to measure influence of too high injection frequency on effectively measured charge.
'''

import os
import time
import copy
import yaml
import numpy as np
import tables as tb
from tqdm import tqdm

import matplotlib.pyplot as plt

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


thresholdscan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 650,
    'VCAL_HIGH_stop': 850,
    'VCAL_HIGH_step': 10
}

STEPS = list(range(0, 100, 2)) + list(range(100, 501, 10))  # Scan range for wait_cycles

WAIT_TIME_OFFSET = 20.6e-6  # Measured delay of CAL_INJ pulses at wait_cycles = 0
WAIT_CYCLE_TIME = 85e-9  # Measured time of 1 wait_cylcle


# Need to define a new ThresholdScan in order to be able to change wait_cycles in each step
class ThresholdScan(ScanBase):
    scan_id = 'threshold_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, TDAC=None, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        TDAC : int / np.ndarray
            If int: TDAC value to use for all enabled pixels, overwriting any existing mask.
            If np.ndarray: TDAC mask to use for enabled pixels. Has to have shape to match start_column:stop_column, start_row:stop_row
        '''
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        if TDAC is not None:
            if type(TDAC) == int:
                self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC
            elif type(TDAC) == np.ndarray:
                self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC[start_column:stop_column, start_row:stop_row]

        self.chip.masks.update(force=True)

    def _scan(self, n_injections=100, VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, **_):
        '''
        Threshold scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        '''

        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)

        wait_cycles = self.configuration['scan']['wait_cycles']

        pbar = tqdm(total=self.chip.masks.get_mask_steps() * len(vcal_high_range), unit=' Mask steps')
        for scan_param_id, vcal_high in enumerate(vcal_high_range):
            self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED)
            with self.readout(scan_param_id=scan_param_id):
                for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
                    if not fe == 'skipped' and fe == 'SYNC':
                        self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections, wait_cycles=wait_cycles)
                    elif not fe == 'skipped':
                        self.chip.inject_analog_single(repetitions=n_injections, wait_cycles=wait_cycles)
                    pbar.update(1)

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            mean_thr = np.median(a.threshold_map[np.nonzero(a.threshold_map)])
            mean_noise = np.median(a.noise_map[np.nonzero(a.noise_map)])
            if np.isfinite(mean_thr):
                self.log.info('Mean threshold is %i [Delta VCAL]' % (int(mean_thr)))
            else:
                self.log.warning('Mean threshold could not be determined!')

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

        return mean_thr, mean_noise


# Main script
if __name__ == '__main__':
    timestamp = time.strftime("%Y%m%d_%H%M%S")
    x, thr_data = [], []
    for wait_cycles in STEPS:
        config = copy.deepcopy(thresholdscan_configuration)
        config['wait_cycles'] = wait_cycles
        with ThresholdScan(scan_config=config) as scan:
            scan.scan()
            scan.analyze()

        try:
            with tb.open_file(scan.output_filename + '_interpreted.h5', 'r') as f:
                thr_dat = np.median(f.root.ThresholdMap[128:264, 0:192])
                noise_dat = np.median(f.root.NoiseMap[128:264, 0:192])
        except tb.exceptions.NoSuchNodeError:
            continue

        x.append(wait_cycles)
        thr_data.append(float(thr_dat))

        output_dir = os.path.abspath(os.path.dirname(scan.output_filename))

        data = {'x': x, 'thr': thr_data}
        outfile = os.path.join(output_dir, timestamp + '_InjectionFrequencyTest_data.yaml')
        with open(outfile, 'w') as f:
            yaml.dump(data, f)

    # ANALYSIS #
    infile = outfile
    output_dir = os.path.dirname(infile)
    output_filename = os.path.basename(infile).split('.')[0] + '.pdf'
    with open(infile, 'r') as f:
        data = yaml.safe_load(f)
        x = data['x'][1:]
        thr_data = data['thr'][1:]

    error = np.std(thr_data)

    # One wait cycle was measured to take 85ns
    def time_function(x):
        return (x * WAIT_CYCLE_TIME + WAIT_TIME_OFFSET) * 1e6

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    fig.subplots_adjust(bottom=0.2)

    ax1.errorbar(x, thr_data, yerr=error, marker='.', ls='none', linewidth=1)

    ax1.set_title('Measured threshold vs. injection frequency')
    ax1.set_xlabel('Wait cycles between injections')
    ax1.set_ylabel('Mean measured threshold [$\Delta$VCAL]')

    ax1.grid()
    ax1.set_xlim(-1 * max(x) * 0.05, max(x) * 1.05)

    # Secondary xaxis
    ax2 = ax1.twiny()
    ax1Xs = ax1.get_xticks()
    ax2Xs = []
    for X in ax1Xs:
        ax2Xs.append(round(time_function(X), 1))

    ax2.set_xticks(ax1Xs)
    ax2.set_xbound(ax1.get_xbound())
    ax2.set_xticklabels(ax2Xs)

    ax2.set_xlabel('Absolute wait time between injections [$\mu$s]')

    # Move secondary xaxis below primary one
    ax2.xaxis.set_ticks_position("bottom")
    ax2.xaxis.set_label_position("bottom")
    ax2.spines["bottom"].set_position(("axes", -0.15))
    ax2.set_frame_on(True)
    ax2.patch.set_visible(False)
    for sp in ax2.spines.values():
        sp.set_visible(False)
    ax2.spines["bottom"].set_visible(True)

    bdaq_default = 300

    plt.annotate('BDAQ53 default', xy=(bdaq_default, 270), xycoords='data', xytext=(150, 273), textcoords='data', color='g',
                 arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=-0.3', color='g'))

    plt.savefig(os.path.join(output_dir, output_filename))
