#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Inject the same charge into an increasing amount of pixels at the same time
    and measure the effectively inejcted charge to visualize overload of the injection circuit.
    Requires TDC to be set up and configured.
'''

import tables as tb
import numpy as np
from tqdm import tqdm

import matplotlib.pyplot as plt

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis


scan_configuration = {
    'VCAL_MED': 500,
    'VCAL_HIGH': 1525
}


class InjectionTest(ScanBase):
    scan_id = 'injection_test'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, VCAL_MED=1000, VCAL_HIGH=4000, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH : int
            VCAL_HIGH DAC value.
        '''

        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)

        # Start CMD LOOP PULSER, 160 MHz
        self.bdaq['pulser_cmd_start_loop'].set_en(True)
        self.bdaq['pulser_cmd_start_loop'].set_width(400)
        self.bdaq['pulser_cmd_start_loop'].set_delay(80)
        self.bdaq['pulser_cmd_start_loop'].set_repeat(1)
        # Configure LEMO mux such that CMD_LOOP_SIGNAL is assigned to LEMO_TX0
        self.bdaq.set_LEMO_MUX(connector='LEMO_MUX_TX0', value=1)

        # Configure all four TDC modules
        self.bdaq.configure_tdc_modules()

        self.chip.masks['enable'][:] = False
        self.chip.masks['hitbus'][:] = False
        self.chip.masks['enable'][128, 0] = True
        self.chip.masks['hitbus'][128, 0] = True
        self.chip.masks['injection'][:] = False
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

    def _scan(self, n_injections=100, **_):
        '''
        Analog scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            VCAL_HIGH DAC value.
        '''

        max_pixels = 26112
        step_size = 136

        pixel_range = list(range(0, 301, 1)) + list(range(301, max_pixels + 1, step_size))

        self.enable_hitor(True)
        self.bdaq.enable_tdc_modules()

        pbar = tqdm(total=len(pixel_range), unit=' pixels')
        for scan_param_id, pixels in enumerate(pixel_range):
            self.chip.masks['injection'][:, :] = False

            rows = int(pixels / 136)
            cols = pixels % 136

            # Enable full rows
            self.chip.masks['injection'][128:264, 0:rows] = True

            # Enable pixels in next row
            self.chip.masks['injection'][128:128 + cols, rows:rows + 1] = True

            self.chip.masks.update()

            self.store_scan_par_values(scan_param_id=scan_param_id, pixles=pixels)
            with self.readout(scan_param_id=scan_param_id):
                self.chip.inject_analog_single(repetitions=n_injections)
            pbar.update(1)

        self.bdaq.disable_tdc_modules()
        self.enable_hitor(False)

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=True, use_tdc_trigger_dist=True, analyze_tdc=True) as a:
            a.analyze_data()

        plot(self.output_filename + '_interpreted.h5')


def plot(in_file):
    with tb.open_file(in_file, 'r') as f:
        hits = f.root.Hits[:]
        scan_param_table = f.root.configuration.scan_params[:]
        scan_param_dict = {v['scan_param_id']: v['pixles'] for v in scan_param_table}

    params, tdc_per_param, tot_per_param = [], {}, {}
    for hit in hits:
        scan_param_id = hit['scan_param_id']
        param = scan_param_dict[scan_param_id]
        tdc_value = hit['tdc_value']
        tot_value = hit['tot']

        if hit['tdc_status'] != 1:
            continue

        if param not in params:
            params.append(param)
        if param not in tdc_per_param.keys():
            tdc_per_param[param] = []
        if param not in tot_per_param.keys():
            tot_per_param[param] = []

        tdc_per_param[param].append(tdc_value)
        tot_per_param[param].append(tot_value)

    tdc = [np.mean(tdc_per_param[param]) for param in params]
    tdc_err = [np.std(tdc_per_param[param]) for param in params]

    tot = [np.mean(tot_per_param[param]) for param in params]
    tot_err = [np.std(tot_per_param[param]) for param in params]

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()

    ax1.errorbar(params, tdc, tdc_err, linestyle='none', marker='.', label='TDC')
    ax2.errorbar(params, tot, tot_err, linestyle='none', marker='.', color='orange', label='ToT')

    ax1.set_xlim(0, 26112)
    ax1.set_ylim(100, 145)
    ax2.set_ylim(4, 13)

    ax1.set_xlabel('Simultaneously injected pixels')
    ax1.set_ylabel('Effectively injected charge [a.u.]')
    ax2.set_ylabel('Mean ToT [ToT code]', color='orange')

    ax1.grid()

    lines, labels = ax1.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax1.legend(lines + lines2, labels + labels2, loc=2)

    inset_axes = ax1.inset_axes([0.5, 0.5, 0.47, 0.47])
    inset_axes.errorbar(params[::5], tdc[::5], tdc_err[::5], linestyle='none', marker='.')
    inset_axes.set_xlim(0, 300)
    inset_axes.set_ylim(122, 132)
    inset_axes.annotate('BDAQ53 default', (136, inset_axes.get_ylim()[0]), (20, 124), color='green',
                        arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=-0.3', color='g'))
    inset_axes.grid()
    ax1.indicate_inset_zoom(inset_axes, label='_nolegend_')

    plt.savefig(in_file.split('.')[0] + '.pdf')


if __name__ == '__main__':
    with InjectionTest(scan_config=scan_configuration) as scan:
        scan.scan()
        scan.analyze()
