# Example how to debug raw data
import os
import tables as tb
import bdaq53  # noqa: E731
from bdaq53.analysis import rd53b_analysis as anb

# Use fixtures
bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))
raw_data_file = os.path.join(data_folder, 'analog_scan_ptot.h5')

# Open data file
with tb.open_file(raw_data_file, mode='r') as in_file_h5:
    raw_data = in_file_h5.root.raw_data[:]
    ptot_table = in_file_h5.root.ptot_table[:]
    ptot_table_stretched = anb.stretch_ptot_data(ptot_table)

# Print raw data
anb.print_raw_data(raw_data=raw_data, ptot_table_stretched=ptot_table_stretched)
